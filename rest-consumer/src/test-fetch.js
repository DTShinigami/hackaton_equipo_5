import { LitElement, html, css } from 'lit-element';

class TestFetch  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planets: {type:Object}
    };
  }

  constructor() {
    super();
    this.planets = { results: [] };
  }

  render() {
    return html`
      ${this.planets.results.map((pl) => {
          return html `<div> ${pl.name} ${pl.rotation_period}</div>`
          
      })}
    `;
  }

  //sirve para que lo llame una vez que se cargue en el contenedor (en este caso index.html)
  connectedCallback(){
      super.connectedCallback();
      try {
          this.cargarPlanetas();
      } catch (error) {
          alert(error);
      }
  }
  cargarPlanetas(){
      fetch("https://swapi.dev/api/planets/")
        .then(response => {
            console.log(response);
            if (!response.ok) { throw response;}
            return response.json();
        })
        .then(data => {
            this.planets = data;
            console.log(data);
        })
        .catch(error => {
            alert("Problemas con el fecth: " + error);
        })
  }
}

customElements.define('test-fetch', TestFetch);
Feature: Lista productos
    Scenario: Cargar lista de productos
        When we request the product list
        Then we should recieve
            | nombre | descripcion |
            | Móvil XL | telefono con una de las mejores pantallas |
            | Móvil Mini | Un telefono mediano con una de las mejores camaras |
            | Móvil Standard | Un telefono Standard. Nada especial. |
            
import { LitElement, html, css } from 'lit-element';

export class OrigenPersona  extends LitElement {

  static get styles() {
    return css`
      select {
        witdh: 150px;
      }
    `;
  }

  static get properties() {
    return {
        origenes:  {type:Array}
    };
  }

  constructor() {
    super();
    this.origenes = ["México","USA","Rusia"];
  }

  render() {
    return html`
      <label>Origen</label>
      <select id="sel" @change="${this.selChange}">
        ${this.origenes.map (i => html `
            <option>${i}</option>
        `)}
        
      </select>
    `;
  }
  selChange(){
      let valor = this.shadowRoot.querySelector("#sel").value;
      let event = new CustomEvent('origen-set', {detail: {message: valor} });

      this.dispatchEvent(event);
  }
}

customElements.define('origen-persona', OrigenPersona);
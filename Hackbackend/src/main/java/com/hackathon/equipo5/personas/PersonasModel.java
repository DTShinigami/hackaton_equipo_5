package com.hackathon.equipo5.personas;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="persona")
public class PersonasModel {
    @NotNull
    private String rfc;
    private String curp;
    private String tipoPersona;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String calle;
    private String numInt;
    private String numExt;
    private Integer codigoPostal;
    private String colonia;
    private String estado;
    private String fechaNacimiento;
    private String ciudadNacimiento;

    public PersonasModel() {
    }

    public PersonasModel(@NotNull String rfc, String curp, String tipoPersona,
                         String nombre, String apellidoPaterno, String apellidoMaterno,
                         String calle, String numInt, String numExt, Integer codigoPostal,
                         String colonia, String estado, String fechaNacimiento, String ciudadNacimiento) {
        this.rfc = rfc;
        this.curp = curp;
        this.tipoPersona = tipoPersona;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.calle = calle;
        this.numInt = numInt;
        this.numExt = numExt;
        this.codigoPostal = codigoPostal;
        this.colonia = colonia;
        this.estado = estado;
        this.fechaNacimiento = fechaNacimiento;
        this.ciudadNacimiento = ciudadNacimiento;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumInt() {
        return numInt;
    }

    public void setNumInt(String numInt) {
        this.numInt = numInt;
    }

    public String getNumExt() {
        return numExt;
    }

    public void setNumExt(String numExt) {
        this.numExt = numExt;
    }

    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCiudadNacimiento() {
        return ciudadNacimiento;
    }

    public void setCiudadNacimiento(String ciudadNacimiento) {
        this.ciudadNacimiento = ciudadNacimiento;
    }

    @Override
    public String toString() {
        return "PersonasModel{" +
                "rfc='" + rfc + '\'' +
                ", curp='" + curp + '\'' +
                ", tipoPersona='" + tipoPersona + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellidoPaterno='" + apellidoPaterno + '\'' +
                ", apellidoMaterno='" + apellidoMaterno + '\'' +
                ", calle='" + calle + '\'' +
                ", numInt='" + numInt + '\'' +
                ", numExt='" + numExt + '\'' +
                ", codigoPostal=" + codigoPostal +
                ", colonia='" + colonia + '\'' +
                ", estado='" + estado + '\'' +
                ", fechaNacimiento='" + fechaNacimiento + '\'' +
                ", ciudadNacimiento='" + ciudadNacimiento + '\'' +
                '}';
    }
}

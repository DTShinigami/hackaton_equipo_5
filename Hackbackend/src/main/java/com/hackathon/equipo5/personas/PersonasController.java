package com.hackathon.equipo5.personas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/hackathon/e5")
public class PersonasController {

    @Autowired
    PersonasServices personasServices;

    @GetMapping("/personas")
    public List<PersonasModel> getPersonas() {
        return personasServices.findAll();
    }
    @GetMapping("/personas/{rfc}")
    public ResponseEntity<PersonasModel> getfiltrado(@PathVariable String rfc){
        if (personasServices.getfiltrado(rfc) == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(personasServices.getfiltrado(rfc));
    }

    @PostMapping("/personas")
    public ResponseEntity<PersonasModel> postPersonas(@RequestBody PersonasModel prod) {
        PersonasModel persona = personasServices.save(prod);
        if(persona == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(persona);
    }


}

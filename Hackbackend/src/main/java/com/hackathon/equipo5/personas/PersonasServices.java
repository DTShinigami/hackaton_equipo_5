package com.hackathon.equipo5.personas;

import com.hackathon.equipo5.citas.CitasModel;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class PersonasServices {


    @Autowired
    PersonasRepository personasRepository;

    public List<PersonasModel> findAll() {
        return personasRepository.findAll();
    }

    public PersonasModel getfiltrado(String rfc){
        return personasRepository.findByRfc(rfc);
    }

    public PersonasModel save(PersonasModel prod) {
        if(getfiltrado(prod.getRfc()) == null){
            return personasRepository.save(prod);
        }
        return null;
    }


}

package com.hackathon.equipo5.personas;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonasRepository extends MongoRepository<PersonasModel, String> {

    public PersonasModel findByRfc(String rfc);
}



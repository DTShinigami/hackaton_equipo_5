package com.hackathon.equipo5.citas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/hackathon/e5")


public class CitasController {

    @Autowired
    CitasServices citasServices;

    @CrossOrigin
    @GetMapping("/citas")
    public List<CitasModel> getCitas(){
        return citasServices.findAll();
    }

    @GetMapping("/citas/{rfc}")
    public ResponseEntity<List<CitasModel>> getCitasByRfc (@PathVariable String rfc){
        if (citasServices.getfiltrado(rfc) == null){
            return ResponseEntity.notFound().build();
        }
            return ResponseEntity.ok().body(citasServices.getfiltrado(rfc));
    }

    @PostMapping("/citas")
    public ResponseEntity<CitasModel> postCitas(@RequestBody CitasModel prod){
        CitasModel var = citasServices.save(prod);
        if(var == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(var);
    }

    @PutMapping("/citas")
    public ResponseEntity<CitasModel> putCitas(@RequestBody CitasModel prod){
        CitasModel var = citasServices.savePut(prod);
        if(var == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(var);
    }

    @DeleteMapping("/citas")
    public boolean deleteCitas(@RequestBody CitasModel prod){
        return citasServices.delete(prod);
    }
}

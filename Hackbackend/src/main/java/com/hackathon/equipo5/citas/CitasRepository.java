package com.hackathon.equipo5.citas;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CitasRepository extends MongoRepository<CitasModel, String> {
    public List<CitasModel> findByRfc(String rfc);
    public CitasModel findByIdCita(String idCita);
}

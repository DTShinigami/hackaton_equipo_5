package com.hackathon.equipo5.citas;

import com.hackathon.equipo5.personas.PersonasModel;
import com.hackathon.equipo5.personas.PersonasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CitasServices {

    @Autowired
    CitasRepository citasRepository;
    @Autowired
    PersonasRepository personasRepository;


    public List<CitasModel> findAll() {
        return citasRepository.findAll();
    }

    public List<CitasModel> getfiltrado(String rfc){
        return citasRepository.findByRfc(rfc);
    }

    public CitasModel save(CitasModel prod) {
        if (personasRepository.findByRfc(prod.getRfc()) == null){
            return null;
        }
        return citasRepository.save(prod);
    }

    public CitasModel savePut(CitasModel prod) {
        if (personasRepository.findByRfc(prod.getRfc()) == null || citasRepository.findByIdCita(prod.getIdCita()) == null){
            return null;
        }
        return citasRepository.save(prod);
    }



    public boolean delete(CitasModel prod){
        try {
            citasRepository.delete(prod);
            return true;
        }catch (Exception ex){
            return false;
        }
    }
}

package com.hackathon.equipo5.citas;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="citas")
public class CitasModel {

    @Id
    @NotNull
    private String idCita;
    private String rfc;
    private String fechaCita;
    private String horaCita;
    private String sucursalCita;

    public CitasModel() {
    }

    public CitasModel(@NotNull String idCita, String rfc, String fechaCita, String horaCita, String sucursalCita) {
        this.idCita = idCita;
        this.rfc = rfc;
        this.fechaCita = fechaCita;
        this.horaCita = horaCita;
        this.sucursalCita = sucursalCita;
    }

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public String getSucursalCita() {
        return sucursalCita;
    }

    public void setSucursalCita(String sucursalCita) {
        this.sucursalCita = sucursalCita;
    }

    @Override
    public String toString() {
        return "CitasModel{" +
                "idCita='" + idCita + '\'' +
                ", rfc='" + rfc + '\'' +
                ", fechaCita='" + fechaCita + '\'' +
                ", horaCita='" + horaCita + '\'' +
                ", sucursalCita='" + sucursalCita + '\'' +
                '}';
    }
}

